<?php

namespace App\Classes;

class Figure
{
    /**
     * @var int
     */
    protected int $id;

    /**
     * @var string
     */
    protected string $name;

    /**
     * @var string
     */
    protected string $authKey;

    function __construct(int $id, string $name, string $authKey) {
        $this->id = $id;
        $this->name = $name;
        $this->authKey = $authKey;
    }

    function getId(): int
    {
        return $this->id;
    }

    function setId(int $newId) {
        $this->id = $newId;
    }

    function getName(): string
    {
        return $this->name;
    }

    function setName(string $newName) {
        $this->name = $newName;
    }

    function getAuthKey(): string
    {
        return $this->authKey;
    }

    function setAuthKey(string $newAuthKey) {
        $this->authKey = $newAuthKey;
    }
}
<?php

namespace App\Classes;

require_once 'Server.php';

class FigureConnector
{
    /**
     * @var Figure
     */
    protected $figure;

    /**
     * @var int
     */
    protected $boxId;

    public function __construct(Figure $figure, int $boxId)
    {
        $this->figure = $figure;
        $this->boxId = $boxId;
    }

    /**
     * @return Figure
     */
    public function getFigure(): Figure
    {
        return $this->figure;
    }

    /**
     * @param Figure $figure
     */
    public function setFigure(Figure $figure)
    {
        $this->figure = $figure;
    }

    /**
     * @return int
     */
    public function getBoxId(): int
    {
        return $this->boxId;
    }

    /**
     * @param int $boxId
     */
    public function setBoxId(int $boxId)
    {
        $this->boxId = $boxId;
    }

    /**
     * @return bool
     */
    public function isValidFigure(): bool
    {
        if(is_null($this->figure)) {
            return false;
        }

        return true;
    }

    /**
     * @return string - contains the path to audio file
     */
    public function getData()
    {
        $figureId = $this->figure->getId();
				return Server::getPackageData($figureId);

        // $projectDir = dirname(__FILE__, 1);
				//
        // if($figureId === 1) {
        //     return printf('%s/data/BestOfMoneyBoyFreestyles.mp3', $projectDir);
        // } else if($figureId === 2) {
        //     return printf('%s/data/MoneyBoy-RapUp2020.mp3', $projectDir);
        // } else if($figureId === 3) {
        //     return printf('%s/data/YungIsvvc-GummibärenDesignerBande.mp3', $projectDir);
        // } else {
        //     return '~/data/RickRoll.mp3';
        // }
    }
}

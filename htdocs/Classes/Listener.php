<?php

namespace App\Listener;

use App\Classes\Figure;

class Listener
{
    private ?Figure $figure;

    public function getFigure(): ?Figure
    {
        $request_json = $_POST['data'];
        $request = json_decode($request_json);
        $name = $request->name;
        $authKey = $request->authKey;

        if ($name && $authKey && !$this->figure) {
            $this->setFigure();
        }

        return null;
    }

    public function remove(): void
    {
        $this->setFigure(null);
    }

    /**
     * @param Figure|null $figure
     */
    public function setFigure(?Figure $figure): void
    {
        $this->figure = $figure;
    }
}
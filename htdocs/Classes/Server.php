<?php

namespace App\Classes;

/**
 * Server Class which handles all XHR Requests
 *
 * @author Florian Voigtländer
 * @since  12.10.2022
 */
class Server {
	/**
	 * Validates if the figure is valid
	 *
	 * @return bool
	 */
	function validateFigure(String $figureID, String $authCode, String $boxID): bool
	{
		return $figureID && $authCode && $boxID;
	}

	/**
	 * Returns a json object with the needed data (metadata, audio data, ...)
	 *
	 * @param String $figureID Figur
	 *
	 * @return String
	 */
	static function getPackageData(String $figureID): String
	{
		if (!in_array($figureID, self::getAllFigures()))
			return '"Invalid figureID"';

		$audio_file = match ($figureID) {
			'001' => '/assets/audio/auferstanden.mp3',
			'002' => '/assets/audio/klingelstreich.mp3',
			'003' => '/assets/audio/zijaretten.mp3',
			'004' => '/assets/audio/MoneyBoy-RapUp2020.mp3',
		};

		$audio_length = match($figureID) {
			'001' => '310',
			'002' => '212',
			'003' => '157',
			default => '',
		};

		$package_data = [
			'audio_file' => $audio_file,
			'length' => $audio_length,
			'file_format' => 'mp3',
		];

		return json_encode($package_data);
	}

	static function getAllFigures(): array
	{
		return ['001', '002', '003', '004'];
	}
}

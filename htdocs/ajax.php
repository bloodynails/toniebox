<?php
/**
 * Schnittstelle zum Server
 *
 * @author Florian Voigtländer
 * @since  26.10.2022
 */

require_once __DIR__ . '/Classes/Server.php';

use App\Classes\Server;

$raw_input_data = file_get_contents('php://input');

$request_obj = json_decode($raw_input_data);

$action = $request_obj->action;
$figure_id = $request_obj->figure_id;

try {
	echo Server::getPackageData($figure_id);
} catch (Error $e) {
	echo $e;
}

const _ = function (selector, parent = null)
{
	return (parent ? parent : document).querySelector(selector)
}

const __ = function(selector, parent = null)
{
	return Array.prototype.slice.call((parent ? parent : document).querySelectorAll(selector))
}

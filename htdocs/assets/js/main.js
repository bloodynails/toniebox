/**
 * main literal
 *
 * @author Florian Voigtländer
 * @since  26.10.2022
 */
const toniebox =
{
	/**
	 * Runs on window.load
	 */
	init()
	{
		const figures = __('[data-figure]')
		const reset_figure = _('[data-figure-reset]')

		if (!figures || !reset_figure)
			return

		figures.forEach(figure => {
			figure.addEventListener('click', e => {
				const figure = e.target
				const figure_id = figure.dataset.figure

				this.dispatchStart(figure_id)
			})
		})

		reset_figure.addEventListener('click', e => {
			this.stopAudio()
		})
	},

	/**
	 * Pause the current audio inside the player
	 */
	stopAudio()
	{
		const audio_element = _('[data-audio]')

		if (!audio_element)
			return

		audio_element.pause();
	},

	/**
	 * Changes the audio src of the player to the given file
	 *
	 * @param {string} audio_file
	 */
	changeAudio(audio_file)
	{
		const audio_element = _('[data-audio]')

		if (!audio_element)
			return

		audio_element.src = audio_file
		audio_element.play()
	},

	/**
	 * Figure gets placed on box, start playing the corresponding audio file
	 *
	 * @param {string} figure_id
	 */
	dispatchStart(figure_id)
	{
		fetch(`${window.location.href}ajax.php`, {
			method: 'POST',
			body: JSON.stringify({action: 'play', figure_id: figure_id})
		}).then(rsp => {
			rsp.json().then(json => {
				this.changeAudio(json.audio_file)
			})
		})
	},
}

toniebox.init()
